from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from pets.models import Pet, PetForm

@login_required
def home(request):
    if request.user.is_authenticated:
        pets = Pet.objects.filter(owner_id=request.user.id)
        total_pets = 0
        for pet in pets:
            total_pets += pet.unit
        if request.method == 'POST':
            for pet in pets:
                pet.delete()
            form1 = PetForm()
            form2 = PetForm()
            pet1 = form1.save(commit=False)
            pet1.unit = request.POST['form1-unit']
            pet1.color = request.POST['form1-color']
            pet1.animal_type = request.POST['form1-animal_type']
            pet1.owner_id = request.user.id
            pet1.save()

            pet2 = form2.save(commit=False)
            pet2.unit = request.POST['form2-unit']
            pet2.color = request.POST['form2-color']
            pet2.animal_type = request.POST['form2-animal_type']
            pet2.owner_id = request.user.id
            pet2.save()
            return HttpResponseRedirect(reverse("home"))
        else:
            form1 = PetForm(prefix="form1")
            form2 = PetForm(prefix="form2")
        return render(request, 'home.html', {'form1': form1,
                                             'form2': form2,
                                             'pets': pets,
                                             'total_pets': total_pets})
