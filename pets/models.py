from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm

COLORS = (
    ('BN', 'brown'),
    ('BK', 'black'),
    ('WT', 'white'),
    ('GY', 'gray'),
)

ANIMAL_TYPES = (
    ('dog', 'dogs'),
    ('cat', 'cats'),
    ('bird', 'birds'),
    ('rabbit', 'rabbits'),
    ('hamster', 'hamsters'),
)

class Pet(models.Model):
    unit = models.IntegerField(default=0)
    color = models.CharField(max_length=2, default='BN', choices=COLORS)
    animal_type = models.CharField(max_length=10, default='dog', choices=ANIMAL_TYPES)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.color + self.animal_type

class PetForm(ModelForm):
    class Meta:
        model = Pet
        fields = ['unit', 'color', 'animal_type']
